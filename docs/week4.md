From your chosen data format, create an output results page.
Use templating that will show the results from some dummy-data. 
The dummy-data is just a list of values as you expect them to occur in your data-format
No need (yet) to process actual uploaded files.

Some example code is given provided by the teacher. See the scripts:
* *basic_Flask_app.py*
* *Flask_app_without_templating.py*
* *Flask_app_with_templating.py*

For more advanced Jinja and CSS inclusion see:
* *Jinja2_inheritance.py*


in the */lib* directory.




**Briefly:** 

Lines of Jinja code between {% ... %}

variables between {{...}}

For information about Jinja templating see [Jinja](http://jinja.pocoo.org)


You also want to use **matplotlib** from python for easy and nice graph generation. See [matplotlib](https://matplotlib.org) for more information