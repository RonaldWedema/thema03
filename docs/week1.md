## Week1
During this week you will investigate the different data formats presented during the lecture and create a project proposal. Pick a data format (either: FASTA, FASTQ, BED, GFF or PDB) before proceeding with the project proposal
and discuss this with your supervisor. At the end of the week you will need to hand in and present the project proposal. 

The project proposal should have the following items described in detail:

* Background (describe opportunity or problem the project is addressing)
* Objectives (these should be measurable and specific)
* Type of visualization (bar-plot, pie-chart, scatter-plot, etc)
* Scope (what is the desired end product of the project, are there multiple phases?)
* Timeframe (when will each phase start/stop, what is the goal of each phase)
* Monitoring and evaluation (describe how the project goal should be evaluated, same for the end product)
