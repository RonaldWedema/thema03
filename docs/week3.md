Finally, time to add some dynamic content to your site!

This week you will create a HTML page with a form that allows an user to upload visualization settings to the server. 
The user should get instructions on what he/she can fill in the form fields.

For details over the use of *forms* see the presentation "HTML5" on Blackboard and [W3schools](https://www.w3schools.com/html/html_forms.asp)

Some example code is given provided by the teacher. See the scripts:
* *basic_Flask_app.py*
* *simple_routing.py*
* *form_handling.py*
* *Flask_static_folder.py*
  
 in the */lib* directory.